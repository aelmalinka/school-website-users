'use strict';

const {
	Application,
	BadRequest,
	NotAuthenticated,
	NotAuthorized,
	NotFound,
	Conflict,
} = require('Application');
const { Pool } = require('pg');
const { User } = require('User');
const config = require('./config.js');
const debug = require('debug')('http');

const app = module.exports = new Application();

const users = {
	check: async (ctx) => {
		if(!await ctx.user.check(ctx))
			throw new NotAuthenticated();
	},
	login: async (ctx) => {
		const {
			name,
			pass,
		} = ctx.req.body;
		debug(`${name} attemping to login`);
		const res = await ctx.pool.query(
			'INSERT INTO users.sessions SELECT name FROM users.users ' +
			'WHERE name = $1::text AND hash = crypt($2::text, hash) ' +
			'RETURNING name, key',
			[name, pass]
		);
		if(res.rowCount == 1 && res.rows[0].name == name) {
			ctx.res.body = {
				name: res.rows[0].name,
				key: res.rows[0].key,
			};
		} else {
			throw new NotAuthorized();
		}
	},
	logout: async (ctx) => {
		await users.check(ctx);
		await ctx.pool.query('DELETE FROM users.sessions WHERE name = $1::text', [ctx.user.name]);
		ctx.res.code = 204;
	},
	get: async (ctx) => {
		await users.check(ctx);
		if(!(await ctx.user.getPermission(ctx.pool)).users.view)
			throw new NotAuthorized();
		const name = decodeURI(ctx.req.url.split('/')[1]);
		if(typeof name === 'undefined')
			throw new BadRequest();
		ctx.res.body = {
			name: name
		};
		const promises = [
			ctx.pool.query(
				'SELECT * FROM users.permission WHERE name = $1::text',
				[name]
			).then((res) => {
				if(res.rowCount == 1)
					ctx.res.body = res.rows[0];
				else {
					debug(`found ${res.rowCount} records`);
					debug(res.rows);
					throw new NotFound();
				}
			}),
			ctx.pool.query(
				'SELECT lmod FROM users.sessions WHERE name = $1::text',
				[name]
			).then((res) => {
				ctx.res.body.sessions = res.rows;
			})
		];
		await Promise.all(promises);
	},
	all: async (ctx) => {
		await users.check(ctx);
		if(!(await ctx.user.getPermission(ctx.pool)).users.list)
			throw new NotAuthorized();
		const { rows } = await ctx.pool.query('SELECT name FROM users.users WHERE hash IS NOT NULL;');
		ctx.res.body = rows;
	},
	sessions: async (ctx) => {
		await users.check(ctx);
		const { rows } = await ctx.pool.query('SELECT * FROM users.sessions WHERE name = $1::text', [ctx.user.name]);
		ctx.res.body = rows;
	},
	getPermIds: async (client, data) => {
		const def = await User.getDefPerm(client);
		const ret = [];
		const promises = [];
		for(const appname in data) {
			for(const key in data[appname]) {
				if(data[appname][key] != def[appname][key]) {
					promises.push(client.query(
						'SELECT id FROM users.permissions, ' +
						'LATERAL jsonb_each(data) ' +
						'WHERE value::boolean = $1::boolean AND ' +
						'app = $2::text AND ' +
						'key = $3::text',
						[data[appname][key], appname, key]
					).then((res) => {
						ret.push(res.rows[0].id);
					}));
				}
			}
		}
		await Promise.all(promises);
		return ret;
	},
	create: async (ctx) => {
		await users.check(ctx);
		if(!(await ctx.user.getPermission(ctx.pool)).users.add)
			throw new NotAuthorized();
		const {
			name,
			pass,
			permissions,
		} = ctx.req.body;
		debug(`${ctx.user.name} attempting create of ${name}`);
		if(
			typeof name !== 'undefined' &&
			typeof pass !== 'undefined' &&
			name !== '' &&
			pass !== '' &&
			name !== 'null'
		) {
			const client = await ctx.pool.connect();
			try {
				await client.query('BEGIN');
				const { rows } = await client.query(
					'SELECT users.create($1::text, $2::text)',
					[name, pass]
				);
				if(rows[0].create == name) {
					if(typeof permissions !== 'undefined') {
						debug('new permissions');
						debug(permissions);
						const ids = await users.getPermIds(client, permissions);
						const promises = [];
						for(const x in ids) {
							promises.push(client.query(
								'INSERT INTO users.users_x_permissions VALUES($1::text, $2::integer)',
								[name, ids[x]]
							));
						}
						await Promise.all(promises);
					}
					await client.query('COMMIT');
					ctx.res.code = 201;
					ctx.res.body = name;
				}
			} catch(e) {
				await client.query('ROLLBACK');
				throw e;
			} finally {
				client.release();
			}

			if(ctx.res.code == 404)
				throw new Error('Failed to create user (no sql error)');
		} else {
			throw new BadRequest();
		}
	},
	modify: async (ctx) => {
		await users.check(ctx);
		if(!(await ctx.user.getPermission(ctx.pool)).users.edit)
			throw new NotAuthorized();
		const user = decodeURI(ctx.req.url.split('/')[1]);
		const {
			pass,
			permissions,
		} = ctx.req.body;
		let {
			name,
		} = ctx.req.body;
		debug(`${ctx.user.name} attempting modify of ${user}`);
		if(
			typeof user !== 'undefined' &&
			typeof pass !== 'undefined' &&
			user !== '' &&
			pass !== ''
		) {
			if(typeof name === 'undefined')
				name = user;
			const client = await ctx.pool.connect();
			try {
				await client.query('BEGIN');
				if(user != name) {
					const { rowCount } = await client.query(
						'SELECT * FROM users.sessions WHERE name = $1::text',
						[user]
					);
					if(rowCount != 0)
						throw new Conflict('User is logged in');
				}
				const { rows } = await client.query(
					'SELECT users.update_user($1::text, $2::text, $3::text)',
					[user, name, pass]
				);
				if(rows[0].update_user == name) {
					if(typeof permissions !== 'undefined') {
						debug('new permissions');
						debug(permissions);
						const ids = await users.getPermIds(client, permissions);
						const promises = [];
						for(const x in ids) {
							promises.push(client.query(
								'INSERT INTO users.users_x_permissions VALUES($1::text, $2::integer)',
								[name, ids[x]]
							));
						}
						await Promise.all(promises);
					}
					await client.query('COMMIT');
					ctx.res.body = name;
				}
			} catch(e) {
				await client.query('ROLLBACK');
				throw e;
			} finally {
				client.release();
			}

			if(ctx.res.code == 404)
				throw new Error('Failed to modify user (no sql error)');
		} else {
			throw new BadRequest();
		}
	},
	remove: async (ctx) => {
		await users.check(ctx);
		if(!(await ctx.user.getPermission(ctx.pool)).users.edit)
			throw new NotAuthorized();
		const user = decodeURI(ctx.req.url.split('/')[1]);
		debug(`${ctx.user.name} attempting remove of ${user}`);
		if(
			typeof user !== 'undefined' &&
			user != ''
		) {
			const client = await ctx.pool.connect();
			try {
				await client.query('BEGIN');
				const a = client.query('DELETE FROM users.sessions_real WHERE name = $1::text', [user]);
				const b = client.query('DELETE FROM users.users_x_permissions WHERE "user" = $1::text', [user]);
				await Promise.all([a, b]);
				await client.query('DELETE FROM users.users WHERE name = $1::text', [user]);
				await client.query('COMMIT');
				ctx.res.code = 204;
			} catch (e) {
				await client.query('ROLLBACK');
				throw e;
			} finally {
				client.release();
			}
		} else {
			throw new BadRequest();
		}
	}
};

app.use(async (ctx) => {
	ctx.pool = new Pool(config.database);
});
app.use(async (ctx) => {
	ctx.user = new User();
});
app.use(async (ctx, next) => {
	await next();
	debug(`${ctx.req.method} ${ctx.req.url} ${ctx.res.code}`);
});
app.use(async (ctx, next) => {
	await next();

	try {
		switch(ctx.req.method) {
			case 'GET':
				if(ctx.req.url == '/all')
					await users.all(ctx);
				else if(ctx.req.url == '/logout')
					await users.logout(ctx);
				else if(ctx.req.url == '/sessions')
					await users.sessions(ctx);
				else
					await users.get(ctx);
				break;
			case 'POST':
				if(ctx.req.url == '/login')
					await users.login(ctx);
				else
					await users.modify(ctx);
				break;
			case 'PUT':
				await users.create(ctx);
				break;
			case 'DELETE':
				await users.remove(ctx);
				break;
		}
	} catch (err) {
		if(err instanceof NotAuthenticated) {
			ctx.res.headers['WWW-Authenticate'] = 'Basic realm=Users';
		}

		throw err;
	}
});

app.listen(config.server.port, () => {
	debug(`listening on http://0.0.0.0:${config.server.port}/`);
});
