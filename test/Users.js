const assert = require('assert');
const Users = require('../index.js');
const config = require('../config.js');
const http = require('http');
const { Pool } = require('pg');

describe('Users', function() {
	const port = 9092;
	const pool = new Pool(config.database);
	
	function request(page, options, done, cb) {
		return http.request(`http://localhost:${port}${page}`, options, (res) => {
			if(typeof cb !== 'undefined')
				cb(res);
		}).on('error', (e) => {
			done(e);
		});
	}
	function parse(res, done, cb) {
		res.body = [];
		res.on('data', (chunk) => {
			res.body.push(chunk)
		}).on('end', () => {
			res.body = Buffer.concat(res.body).toString()				
			if(res.headers['content-type'] === 'application/json')
				res.body = JSON.parse(res.body);
			cb(res);
		}).on('error', (e) => {
			done(e);
		});
	}
	function get(page, u, done, cb) {
		const options = {
			method: 'GET',
		};
		if(u.key !== '')
			options.headers = {
				'authorization': u.header,
			};
		return request(page, options, done, cb).end();
	}
	function del(page, u, done, cb) {
		const options = {
			method: 'DELETE',
		};
		if(u.key !== '')
			options.headers = {
				'authorization': u.header,
			};
		return request(page, options, done, cb).end();
	}
	function put(page, u, data, done, cb) {
		const options = {
			method: 'PUT',
		};
		if(u.key !== '')
			options.headers = {
				'authorization': u.header,
			};
		return request(page, options, done, cb).end(data);
	}
	function post(page, u, data, done, cb) {
		const options = {
			method: 'POST',
		};
		if(u.key !== '')
			options.headers = {
				'authorization': u.header,
			};
		return request(page, options, done, cb).end(data);
	}

	const user = {
		name: 'aladdin',
		pass: 'open sesame',
		key: '',
		get auth() {
			return `${this.name}:${this.key}`;
		},
		get header() {
			return `Basic ${Buffer.from(this.auth).toString('base64')}`;
		}
	};
	
	const user_forbidden = {
		name: 'abu',
		pass: 'eee eee eee ooo ooo ooo',
		key: '',
		get auth() {
			return `${this.name}:${this.key}`;
		},
		get header() {
			return `Basic ${Buffer.from(this.auth).toString('base64')}`;
		}
	};

	before(function(done) {
		Users.close(() => {
			Users.listen(port, () => {
				pool.query(
					'SELECT users.create($1::text, $2::text)',
					[user.name, user.pass]
				).then((res) => {
					pool.query(
						'INSERT INTO users.users_x_permissions ' +
						'SELECT $1::text, id FROM users.permissions, ' +
						'LATERAL jsonb_each(data) WHERE app = $2::text ' +
						'AND jsonb_each.value::bool',
						[user.name, 'users']
					).then(() => {
						done();
					}).catch((e) => done(e));
				}).catch((e) => done(e));
			});
		});
	});
	after(function(done) {
		Users.close(() => {
			pool.query('DELETE FROM users.sessions_real WHERE name = $1::text', [user.name]).then(() => {
				pool.query('DELETE FROM users.users_x_permissions WHERE "user" = $1::text', [user.name]).then(() => {
					pool.query('DELETE FROM users.users WHERE name = $1::text', [user.name]).then(() => {
						done();
					}).catch((e) => done(e));
				}).catch((e) => done(e));
			}).catch((e) => done(e));
		});
	});

	it('should tell unauthenticated users (get /) to auth', function(done) {
		get('/', user, done, (res) => {
			assert.equal(res.statusCode, 401);
			assert.equal(typeof res.headers['www-authenticate'], 'string');
			assert.equal(res.headers['www-authenticate'], 'Basic realm=Users');
			done();
		});
	});
	it('should tell unauthenticated users (get /logout) to auth', function(done) {
		get('/', user, done, (res) => {
			assert.equal(res.statusCode, 401);
			assert.equal(typeof res.headers['www-authenticate'], 'string');
			assert.equal(res.headers['www-authenticate'], 'Basic realm=Users');
			done();
		});
	});
	it('should tell unauthenticated users (delete /) to auth', function(done) {
		del('/', user, done, (res) => {
			assert.equal(res.statusCode, 401);
			assert.equal(typeof res.headers['www-authenticate'], 'string');
			assert.equal(res.headers['www-authenticate'], 'Basic realm=Users');
			done();
		});
	});
	it('should tell unauthenticated users (put /) to auth', function(done) {
		put('/', user, '', done, (res) => {
			assert.equal(res.statusCode, 401);
			assert.equal(typeof res.headers['www-authenticate'], 'string');
			assert.equal(res.headers['www-authenticate'], 'Basic realm=Users');
			done();
		});
	});
	it('should allow users to login', function(done) {
		post('/login', user, JSON.stringify({name: user.name, pass: user.pass}), done, (res) => {
			parse(res, done, (res) => {
				assert.equal(res.statusCode, 200);
				assert.equal(typeof res.body, 'object');
				assert.equal(typeof res.body.key, 'string');
				assert(res.body.key.length != 0);
				user.key = res.body.key;
				done();
			});
		});
	});
	it('should tell users about their sessions', function(done) {
		get('/sessions', user, done, (res) => {
			parse(res, done, (res) => {
				assert.equal(res.statusCode, 200);
				assert.equal(typeof res.body, 'object');
				done();
			});
		});
	});
	it('should allow authorized users to add users', function(done) {
		put('/', user, JSON.stringify({
			name: user_forbidden.name,
			pass: user_forbidden.pass,
			permissions: { users: {
				add: true,
				edit: true,
				remove: true
			}, },
		}), done, (res) => {
			parse(res, done, (res) => {
				assert.equal(res.statusCode, 201);
				assert.equal(typeof res.body, 'string');
				assert.equal(res.body, user_forbidden.name);
				done();
			});
		});
	});
	it('shuold allow authorized users to modify users', function(done) {
		post(`/${user_forbidden.name}`, user, JSON.stringify({
			pass: user_forbidden.pass,
			permissions: { users: {
				add: false,
				edit: false,
				remove: false,
			}, },
		}), done, (res) => {
			parse(res, done, (res) => {
				assert.equal(res.statusCode, 200);
				assert.equal(typeof res.body, 'string');
				assert.equal(res.body, user_forbidden.name);
				done();
			});
		});
	});
	it('should allow users to logout', function(done) {
		get('/logout', user, done, (res) => {
			parse(res, done, (res) => {
				assert.equal(res.statusCode, 204);
				user.key = '';
				done();
			});
		});
	});
	it('should let permissionless users login', function(done) {
		post('/login', user_forbidden, JSON.stringify({
			name: user_forbidden.name,
			pass: user_forbidden.pass
		}), done, (res) => {
			parse(res, done, (res) => {
				assert.equal(res.statusCode, 200);
				assert.equal(typeof res.body, 'object');
				assert.equal(typeof res.body.key, 'string');
				assert(res.body.key.length != 0);
				user_forbidden.key = res.body.key;
				done();
			});
		});
	});
	it('should reject unauthorized users from adding users', function(done) {
		put('/', user_forbidden, JSON.stringify({
			name: 'asdf',
			pass: 'asdf',
		}), done, (res) => {
			assert.equal(res.statusCode, 403);
			done();
		});
	});
	it('should reject unauthorized users from modifying users', function(done) {
		post('/user', user_forbidden, JSON.stringify({
			name: 'asdf',
			pass: 'asdf',
		}), done, (res) => {
			assert.equal(res.statusCode, 403);
			done();
		});
	});
	it('should reject unauthorized users from deleting users', function(done) {
		del('/user', user_forbidden, done, (res) => {
			assert.equal(res.statusCode, 403);
			done();
		});
	});
	it('should still let said user logout', function(done) {
		get('/logout', user_forbidden, done, (res) => {
			parse(res, done, (res) => {
				assert.equal(res.statusCode, 204);
				user_forbidden.key = '';
				done();
			});
		});
	});
	it('should allow authorized users to delete users', function(done) {
		post('/login', user, JSON.stringify({
			name: user.name,
			pass: user.pass,
		}), done, (res) => {
			parse(res, done, (res) => {
				user.key = res.body.key;
				del(`/${user_forbidden.name}`, user, done, (res) => {
					parse(res, done, (res) => {
						assert.equal(res.statusCode, 204);
						get('/logout', user, done, (res) => {
							done();
						});
					});
				});
			});
		});
	});
});
